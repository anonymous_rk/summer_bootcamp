from django.db import models

# Create your models here.


class Profile(models.Model):
    external_id = models.PositiveIntegerField(verbose_name="ID user social network", unique=True)
    first_name = models.CharField(max_length=250, null=True, blank=True)
    last_name = models.CharField(max_length=250, null=True, blank=True)


    def __str__(self):
        return self.first_name
