import random

def make_underscores(word):
    output = list()
    for w in word:
        output.append('__')
    return word, output


def find_occurances(letter, word, occurances=[]) -> list:
    for i in range(len(word)):
        if (word[i].lower() == letter.lower()):
            occurances.append(i)
    return occurances

def guess_letters():
    word, lst = make_underscores(rand_word)
    guess_limit = 10

    print(f'You have to find {len(word)} letters in {guess_limit} chances. Here we go!')
    print(" ".join(lst))


    while guess_limit > 0:

        if '__' not in lst:
            print(f"Congratulations! You find the word! It is '{word}'")
            return

        letter = input(f'{guess_limit} chances left. Guess letter: ')
        if letter.lower() not in word.lower():
            print('Your letter does not match. Please try again.')
            print(" ".join(lst))
        elif letter.lower() in word.lower():
            occurances = find_occurances(letter=letter, word=word, occurances=[])
            for i in occurances:
                lst[i]=word[i]
            print(" ".join(lst))

        guess_limit -= 1
    print(f"Sorry you find only these letters {lst}")
    return