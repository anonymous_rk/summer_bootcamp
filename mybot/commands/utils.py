from mybot.models import Profile
__all__ = ['get_chat']


def get_chat(update):
    # print("update", update.message.from_user)
    chat_instance, is_created = Profile.objects.get_or_create(
        external_id=update.message.chat.id,
        defaults={"first_name": update.message.from_user.first_name,
                  "last_name": update.message.from_user.last_name})
    return chat_instance
