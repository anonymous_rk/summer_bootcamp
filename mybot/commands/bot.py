#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This program is dedicated to the public domain under the CC0 license.

"""
Simple Bot to reply to Telegram messages.

First, a few handler functions are defined. Then, those functions are passed to
the Dispatcher and registered at their respective places.
Then, the bot is started and runs until we press Ctrl-C on the command line.

Usage:
Basic Echobot example, repeats messages.
Press Ctrl-C on the command line or send a signal to the process to stop the
bot.
"""

import logging
import random
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters

REQUEST_KWARGS = {
    'proxy_url': 'http://192.168.7.251:3128',
}
# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)

logger = logging.getLogger(__name__)

WORDS_LIST = ['contribution', 'message', 'telegram', 'laptop', 'Lobar', 'love', 'coffee', 'girlfriend', 'boyfriend', 'computer', 'command', 'people', 'sentence', 'picture', 'country', 'thought', 'together', 'mountain', 'enough', 'family', 'problem', 'remember', 'village', 'century']  # custom words list to use




# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    global random_word, word_mask, lives
    """Send a message when the command /start is issued."""
    random_word = random.choice(WORDS_LIST)
    word_mask = ['*' for i in range(len(random_word))]
    update.message.reply_text('Game is started.\n'
                              f'Secret word: {" ".join(word_mask)}\n'
                              'You have 5 lives ❤️❤️❤️❤️❤️\n'
                              'Type guess letters. Good luck!✊')

    return random_word


def find_occurances(letter, word, occurances=[]) -> list:
    for i in range(len(word)):
        if (word[i].lower() == letter.lower()):
            occurances.append(i)
    return occurances




lives = 5
def guess_word(update, bot):

    global lives
    secret_word, mask = random_word, word_mask

    # update.message.reply_text(f'{msg}')

    # print('letter: ', type(letter))
    print('Secret word: ', random_word)
    print('mask: ', mask)

    # while lives > 0:

    letter = update.message.text
    occurances = find_occurances(letter, secret_word, occurances=[])
    if letter.lower() not in secret_word.lower():
        lives -= 1
        if lives == 0:
            update.message.reply_text('You have no lives left.😕')
            return update.message.reply_text(f'Sorry you failed.☹️☹☹️\n Secret word was: ️{secret_word}\n')
        update.message.reply_text(f"Wrong letter. You have {lives * '️❤'} lives left.🥵")
        print(lives)

    elif letter.lower() in secret_word.lower():
        print('in')
        for i in occurances:
            print(i)
            print(mask[i])
            mask[i] = letter
            print(mask)

        if "".join(mask) == secret_word:
            # print(mask)
            # print('there is *')
            update.message.reply_text(f'Congratulations! You found the secret word:\n {secret_word.upper()}🥳🥳🥳')
            return

        update.message.reply_text(f'{" ".join(mask)}\nGood job. Keep going!☺️')

    if lives == 0 and "*" in mask:
        return update.message.reply_text(f'Sorry you failed.☹️☹☹️\n Secret word was: ️{secret_word}\n')


# def stop(update, context):
#     context.job_queue.stop()


def help(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text('Help!')


def error(update, context):
    """Log Errors caused by Updates."""
    print(context.error)
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def shutdown():
    updater.stop()
    updater.is_idle = False


def stop(update, context):
    threading.Thread(target=shutdown).start()


def main():
    """Start the bot."""
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("1684010718:AAF2gupcX_Wg2b5-3cDSVS9DWok2xSMqHqo", use_context=True,
                      request_kwargs=REQUEST_KWARGS
                      )

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # on different commands - answer in Telegram
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("help", help))

    # on noncommand i.e message - echo the message on Telegram


    dp.add_handler(CommandHandler('stop', stop))

    dp.add_handler(MessageHandler(Filters.text, guess_word))
    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
