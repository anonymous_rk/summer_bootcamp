# Solution - Needed number of trees for  'Частокол'

def get_variables(file, N=0, M=0, H=0):
    with open(str(file), 'r') as f:
        lines = f.readlines()

    for line in lines:
        if line.startswith('N'):
            N = int(line.split('=')[-1])
        elif line.startswith('M'):
            M = int(line.split('=')[-1])
        elif line.startswith('H'):
            H = int(line.split('=')[-1])
    print(N, M, H)
    return [N, M, H]

def find_cutting_trees():
    lst = get_variables('input_1')
    N = lst[0]
    M = lst[1]
    H = lst[2]
    result = (N*M)/H

    output = open('output.txt', 'w')
    output.writelines('\n'.join([f'N={N}', f'M={M}', f'H={H}', f'Needed number of trees: {result}']))
    output.close()

    return output



# Solution -  "Виселица"

import random

words = ['Country', 'Summer', 'England', 'Bootcamp', 'Learning', 'Library', 'External', 'Package', 'Definition', 'Reception', 'Virtual', 'tElegram']
rand_word = random.choice(words)

def make_underscores(word):
    output = list()
    for w in word:
        output.append('__')
    return word, output


def find_occurances(letter, word, occurances=[]) -> list:
    for i in range(len(word)):
        if (word[i].lower() == letter.lower()):
            occurances.append(i)
    return occurances


def guess_letters():
    word, lst = make_underscores(rand_word)
    guess_limit = 10

    print(f'You have to find {len(word)} letters in {guess_limit} chances. Here we go!')
    print(" ".join(lst))


    while guess_limit > 0:

        if '__' not in lst:
            print(f"Congratulations! You find the word! It is '{word}'")
            return

        letter = input(f'{guess_limit} chances left. Guess letter: ')
        if letter.lower() not in word.lower():
            print('Your letter does not match. Please try again.')
            print(" ".join(lst))
        elif letter.lower() in word.lower():
            occurances = find_occurances(letter=letter, word=word, occurances=[])
            for i in occurances:
                lst[i]=word[i]
            print(" ".join(lst))

        guess_limit -= 1
    print(f"Sorry you find only these letters {lst}")
    return



# Solution -


